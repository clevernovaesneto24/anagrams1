function getAnagramsOf(input){
    let typedText = document.getElementById("input").value;
    getAnagramsOf(typedText);
}

const button = document.getElementById("findButton");
button.onclick = function () {
   // seu código vai aqui
   let findButton = document.querySelector("#findButton");
   findButton.addEventListener('click', getAnagramsOf)
}

function alphabetize(palavras) {
    return palavras.toLowerCase().split("").sort().join("").trim();
}